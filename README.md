This is a trivial repository-based Cloudlab profile. 
The experiment topology is defined using a geni-lib script, "profile.py".

If you have software to install/run, you could put that into this repository,
it will be cloned to each of the nodes in your experiment.

The profile that is based on this repository can be found at https://www.cloudlab.us/p/PortalProfiles/RepoBased

EOF
